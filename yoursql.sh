#!/bin/bash

needhelp=1
overwrite=''

manpage="
	                 _____ _____ __    
	 _ _ ___ _ _ ___|   __|     |  |   
	| | | . | | |  _|__   |  |  |  |__ 
	|_  |___|___|_| |_____|__  _|_____|
	|___|                    |__|       (Riley's tool)

	Wrapper to the 'mysql' and 'mysqldump' programs to import and export databases, as well as drop.

	Syntax:
 		yoursql mode [db user pass] [host] file


 		However if db, user, pass and host are already stored: 

 		yoursql mode file
	
	Modes:
		-e (export)
		-i (import)
		-d (drop)
		-h (displays this manly page)
		--reset (clears stored variables)
	
	Examples:
		yoursql -e db_name user_name 'p@ssw0rd!' path/to/file.sql
		yoursql -e db_name user_name 'p@ssw0rd!' hostaddress.something.com path/to/file.sql
		yoursql -e path/to/file.sql

		yoursql -i db_name user_name 'p@ssw3ird' path/to/file.sql
		yoursql -i db_name user_name 'p@ssw3ird' mysql.somethingelse.com path/to/file.sql
		yoursql -i path/to/file.sql

		yoursql -d db_name user_name 'P@ssw-rd'
		yoursql -d

		yoursql --reset

	Notes:
		The first time you enter db, user, pass & host, those get stored. They get cleared if you change directories and run again or use the --reset option

		If you don't want to specify host, it defaults to localhost


"

clear_your_s_q_l() {
	echo "resetting..."
	yoursqldb=""
	yoursqluser=""
	yoursqlpass=""
	yoursqlhost=""
}

yoursql() {

	mode="$1"
	presentdir=`pwd`

	if [[ "$presentdir" != "$previousdir" ]]; then
		echo "Working directory changed!"
		clear_your_s_q_l
		previousdir="$presentdir"
	fi

	if [[ -z "$mode" ]]; then
		# ((needhelp++))
		# if [[ "$needhelp" -ge 5 ]]; then
		# 	needhelp=0
		# 	echo "Need help? Run 'mysql -h'"
		# else
		# 	echo "No mode specified "
		# fi
		# return
		echo -e "$manpage"
		return

	elif [[ "$mode" == "--reset" ]]; then
		clear_your_s_q_l
	elif [[ "$mode" == "-h" ]]; then
		echo -e "$manpage"
		return
	elif [[ "$mode" == "-d" ]]; then

		if [[ "$presentdir" != "$previousdir" ]]; then
			echo "Working directory changed!"
			clear_your_s_q_l
			previousdir="$presentdir"
		fi

		if [[ -z "$2" ]]; then
			if [[ -z "$yoursqldb" || -z "$yoursqluser" || -z "$yoursqlpass" ]]; then
				echo "No stored values to use, please enter full set of arguments"
				return
			else
				echo "Using stored values"
			fi

		elif [[ -z "$4" ]]; then
			echo "Incomplete argument set"
			return
		else
			yoursqldb="$2"
			yoursqluser="$3"
			yoursqlpass="$4"
			if [[ -n "$5" ]]; then
				yoursqlhost="$5"
			else
				yoursqlhost="localhost"
			fi
		fi
		read -p "You made a backup? " -n 1 yoursqlgarbage
		if [[ "$yoursqlgarbage" =~ [Yy] ]]; then
			query='DROP database '$yoursqldb'; CREATE DATABASE '$yoursqldb';'
			mysql -p"$yoursqlpass" -u "$yoursqluser" "$yoursqldb" -h "$yoursqlhost" -e "$query" && echo "DB dropped"
		else
			echo -e "\nAbort"
		fi
	

	else
		if [[ "$mode" == '-i' || "$mode" == '-e' ]]; then
			if [[ "$presentdir" != "$previousdir" ]]; then
			echo "Working directory changed!"
			clear_your_s_q_l
			previousdir="$presentdir"
			fi
			

			if [[ -z "$3" ]]; then # that means you're using "short" notation
				if [[ -z "$2" ]]; then
					if [[ -z "$yoursqldb" || -z "$yoursqluser" || -z "$yoursqlpass" ]]; then
						echo "No file specified, no stored values"
						return
					else
						echo "No file specified"
						return
					fi
				elif [[ -z "$yoursqldb" || -z "$yoursqluser" || -z "$yoursqlpass" ]]; then
					echo "No stored values to use, please enter full set of arguments"
					return
				else
					yoursqlfile="$2"
					echo "Using stored values"
				fi
				
			elif [[ -z "$5" ]]; then # you must be using the full "long" notation and don't have a full set of arguments. 
				echo "Incomplete argument set"
				return
			else # you've got a full set of arguments
				yoursqldb="$2"
				yoursqluser="$3"
				yoursqlpass="$4"

				if [[ -z "$6" ]]; then # you seem to have entered a host 
					yoursqlfile="$5"
				else
					yoursqlhost="$5"
					yoursqlfile="$6"
				fi

			fi

			if [[ "$mode" == '-e' ]]; then
				echo "$yoursqlfile"
				du -h "$yoursqlfile"
				cat "$yoursqlfile"
				if [[ -e "$yoursqlfile" && -s "$yoursqlfile" ]]; then
					read -p "File exists and contains data. Remove current? (y for yes, any key for no) " overwrite
					echo "Overwrite: $overwrite"
					if [[ "$overwrite" == 'y' || "$overwrite" == 'Y' ]]; then
						rm "$yoursqlfile"
						echo "$yoursqlfile removed"
						overwrite=''
					else
						echo "aborting"
						return
					fi
				fi
							
				echo "exporting..."
				mysqldump -p"$yoursqlpass" -u "$yoursqluser" "$yoursqldb" -h "$yoursqlhost"> "$yoursqlfile" && echo "exported $yoursqldb to $yoursqlfile"
				
				
			elif [[ "$mode" == '-i' ]]; then
				echo "importing from $yoursqlfile"
				mysql -p"$yoursqlpass" -u"$yoursqluser" "$yoursqldb" -h "$yoursqlhost" < "$yoursqlfile" && echo "imported to $yoursqldb from $yoursqlfile"
			else
				echo "Invalid mode. Try 'yoursql -h' for info"
			fi	
		else
			echo "Invalid mode. Try 'yoursql -h' for info"
		fi
	fi

	previousdir="$presentdir"
	
}



echo -e "\n 	yourSQL script injected. run 'yoursql -h' for info.\n"